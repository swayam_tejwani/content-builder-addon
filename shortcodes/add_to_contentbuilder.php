<?php

require_once(plugin_dir_path(__FILE__).'builder_shortcodes_extended/contact-extended/contact-extended.php');

add_action( 'ig_pb_addon', 'contentbuilder_custom_addon');

function contentbuilder_custom_addon(){
	
	class PT_Shortcodes_CUSTOM extends IG_Pb_Addon {

		public function __construct() {

			// setup information
			$this->set_provider(
				array(
					'name' => 'Custom Theme Zone',
					'file' => __FILE__,
					'shortcode_dir' => 'builder_shortcodes_extended'
				)
			);

			//$this->custom_assets();

			// call parent construct
			//parent::__construct();
		}	
		
		// regiter & enqueue custom assets
		public function custom_assets() {
			// register custom assets
			$this->set_assets_register(
				array(
					'ig-frontend-free-css' => array(
						'src' => plugins_url( 'assets/css/main.css' , dirname( __FILE__ ) ),
						'ver' => '1.0.0',
					),
					'ig-frontend-free-js' => array(
						'src' => plugins_url( 'assets/js/main.js' , dirname( __FILE__ ) ),
						'ver' => '1.0.0',
					)
				)
			);
			// enqueue assets for Admin pages
			$this->set_assets_enqueue_admin( array( 'ig-frontend-free-css' ) );
			// enqueue assets for Modal setting iframe
			$this->set_assets_enqueue_modal( array( 'ig-frontend-free-js' ) );
			// enqueue assets for Frontend
			$this->set_assets_enqueue_frontend( array( 'ig-frontend-free-css', 'ig-frontend-free-js' ) );
		}
	
	}
	
	$this_ = new PT_Shortcodes_CUSTOM();
	
}