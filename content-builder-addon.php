<?php
/*
Plugin Name: Content Builder Addon
Author: Swayam Tejwani
Author URI: http://google.com
Description: Addon for extending dashstore content builder.
Version: 1.0
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if (!class_exists('IG_Pb_Init')) 
	return;

require_once( plugin_dir_path(__FILE__) . '/shortcodes/add_to_contentbuilder.php');